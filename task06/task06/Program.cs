﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(number(1));
            Console.WriteLine(number(2));
            Console.WriteLine(number(3));
            Console.WriteLine(number(4));
            Console.WriteLine(number(5));
        }
       
        static string number(int number)        
        {
            var output = string.Empty;
            if (number % 2 == 0)
            {
               output = ("This number is even");
            }     
            else
            {
               output = ("This number is false");
            }        
            return output;
        }
    }
}
